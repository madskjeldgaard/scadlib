/* This is based on this push button:
 * https://www.taydaelectronics.com/push-button-panel-mount-green-knob-spst-no.html
 *
 */

// Legs
leglen=5.9; legw=2; legthick=0.6;

// Base
baseh=6.32; basedia=7.42; // "base"
pph=2.42; ppdia=9.42; // "prepanel"

function pushb1HeightToPanel()=leglen+baseh+pph;

module pushbutton1(withLegs=true){

	// Base
	color("black") cylinder(h=baseh,d=basedia,center=false);

	// Prepanel
	color("grey") translate([0,0,baseh]) cylinder(h=pph,d=ppdia,center=false);

	// The part where the bolt fastens (excluding the bolt)
	bolth=6.75; boltdia=6.75;
	color("blue") translate([0,0,baseh+pph]) cylinder(h=bolth,d=boltdia,center=false);

	// Pushey part
	pushh=5.87; pushdia=5.9;
	color("green") translate([0,0,baseh+pph+bolth]) cylinder(h=pushh,d=pushdia,center=false);

	if(withLegs){
		/* color("grey") rotate([0,0,90]) translate([(basedia/2-legw),0,(-1)*leglen]) cube([legw,legthick,leglen],false); */
		color("grey") rotate([0,0,0]) translate([-legw/2,(basedia/2-legthick)-1.3,-1*leglen]) cube([legw,legthick,leglen],false);
		color("grey") rotate([0,0,180]) translate([-legw/2,(basedia/2-legthick)-1.3,-1*leglen]) cube([legw,legthick,leglen],false);
	};

}
