function teensy32Width()=17.7;
function teensy32Len()=35.5;
function teensy32Thickness()=1.6;
function teensy32USBWidth()=8.0;

module teensy32USBPlug(padding=0, prolong=0){
	uwidth=teensy32USBWidth()+padding;
	uheight=3.05+padding;
	ulen=5.4+padding;

	color("grey")
		translate([(teensy32Width()-uwidth)/2.0, teensy32Len()-ulen+1, 0])
		cube([uwidth, ulen+prolong, uheight], false);
}

module teensy32(padding=0,extraUsbLen=0){
	padding = padding / 2.0;

	/* PCB */
	width=teensy32Width() + padding;
	len=teensy32Len() + padding;
	thick=teensy32Thickness() + padding;
	color("green") cube([width,len,thick],false);

	/* Button */
	butWidth=4.5+padding; butLen=3.2+padding; butHeight=2.3+padding;
	color("white")
		translate([(width-butWidth)/2.0, 2.6, thick])
		cube([butWidth, butLen, butHeight], false);

	/* USB */
	teensy32USBPlug();
}
